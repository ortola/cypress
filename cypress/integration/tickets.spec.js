
describe ("Tickets", () => {
beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"))

//Cenário 01 - Preenchendo dados básicos
it("fills all the text input fields", () => {
    const firstName = "Rafael";
    const lastName = "Ortola";

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("teste@teste.com");
    cy.get("#requests").type("Vegetarian");
    cy.get("#signature").type(`${firstName} ${lastName}`);
});

//Cenário 02 - Selecionando quantidade de tickets
it("select two tickets", () => {
    cy.get("#ticket-quantity").select("2");
});

//Cenário 03 - Selecionando radio buttons
it("select 'vip' ticket type", () => {
    cy.get("#vip").check();
});

//Cenário 04 - Selecionando checkbox
it("selects 'social media' checkbox" , () => {
    cy.get("#social-media").check();
});

//Cenário 05 - Selecinando checkbox e retirando a seleção do checkbox friend
it("selects 'friend', and 'publication', then uncheck 'friend'" , () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();
});

//Cenário 06 - Fazendo assertivas e garantindo que o título é igual a "TICKETBOX"
it("has 'TICKETBOX' header´s heading" , () => {
    cy.get("header h1").should("contain", "TICKETBOX");
});

//Cenário 07 - Validando alerta de email inválido com aliás
it("alerts on invalid email" , () => {
    cy.get("#email")
      .as("email")
      .type("teste-gmail.com");
    
    cy.get("@email")
      .clear()
      .type("teste@teste.com.br");
      
    cy.get("#email.invalid").should("not.exist");
});

//Cenário 08 - teste completo e2e
it("fills and reset the form" , () => {
    const firstName = "Rafael";
    const lastName = "Ortola";
    const fullName = `${firstName} ${lastName}`;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("teste@teste.com");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#friend").check();
    cy.get("#requests").type("IPA beer");

    cy.get(".agreement p").should(
        "contain",
        `I, ${fullName}, wish to buy 2 VIP tickets.`
    );

    cy.get("#agree").click();
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button[type='reset']").click();
    
    cy.get("@submitButton").should("be.disabled");
});   


//Cenário 09 - Comandos customizados no cypress para poder economizar espaço e deixar uma estrutura melhot
it("fiils mandatory fields using support command" , () => {
    const customer = {
        firstName: "Rafael",
        lastName: "Ortola",
        email: "testerafael@gmail.com"
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("#agree").uncheck();
    
    cy.get("@submitButton").should("be.disabled");

});   
});